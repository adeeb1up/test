<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'test3');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~R!a#|X|l%((tk^AsK]-Tilb[I[$zku1=)+2u=HFN,-r!`{gqFaMr[TFMK:_6O-T');
define('SECURE_AUTH_KEY',  '9&_$a1TK`{Hc6ek^fL4a 2s9J[r[m|u{[piTK9xIh^4!WMN}12IdO{a6=z&{wU* ');
define('LOGGED_IN_KEY',    'U5F 7cVd5PT@&xu%<}]y/gIOYp8J3|k0mE%U3pL+hh3u/.29Hi10h]u6D98$p8,2');
define('NONCE_KEY',        'Kzj]<OY/h>d-YR-KfFRAbh{OlHoB75[t:3bm#03.K>> }%9bWLMLd9e5dyn$W5_0');
define('AUTH_SALT',        'bl/**>QZj i-b82go2j&{@e]4Q$%j%-Y6|M7WSV|}|3&My%87jR4O5S/-d&3/:5S');
define('SECURE_AUTH_SALT', '+!LMEm@+E31M{do6po-d)BmI!bKdY/OJF5^14{d) Tn@:z8/+|*+$oSe8p*+6fKK');
define('LOGGED_IN_SALT',   'N|yfDcS8.rzK#p x}ak*Ne,.n^=QSfle-0~V#AANP2*xbn<n mlOsPv=(>d4+P3X');
define('NONCE_SALT',       '*|;h*ZN8Sb0S^CCRNhTTm$H.Vm2M5M05SXv0!z`$z^.hns[G^Y kz,7ciu7T8cxU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
